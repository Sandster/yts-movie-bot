"use strict";

const Discord = require("discord.js");
const FileStructure = require("fs");
const { prefix, token } = require("./config.json");
const Log = require("./utils/log");

const client = new Discord.Client();
client.commands = new Discord.Collection();

const cooldowns = new Discord.Collection();
const TAG = `index.js`;

//Launching bot
client.on("ready", () => {
  Log.addInfo(`Bot on standby...`);
  client.user.setStatus("idle");
  client.user.setActivity(`movies, obviously...`, {
    type: `WATCHING`,
  });
});

const commandFiles = FileStructure.readdirSync("./commands").filter((file) =>
  file.endsWith(`.js`)
);

//Dynamically add all files as commands
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

client.on("message", (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  //Breakdown the message content
  const args = message.content.slice(prefix.length).trim().split(/ +/);
  const commandName = args.shift().toLowerCase();
  const command = client.commands.get(commandName);

  //Validate the command
  if (!client.commands.has(commandName)) {
    message.channel.send(`Invalid command, ${message.author}!`);
    return;
  } else if (!args.length) {
    if (command.argsMandatory) {
      message.channel.send(
        `You didn't provide any arguments for this command, ${message.author}!`
      );
      return;
    }
  }

  //Private messaging disabled
  if (command.guildOnly && message.channel.type === `dm`) {
    return message.reply("I'm not allowed to answer you in DMs, sorry");
  }

  //Message cooldown duration
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  //Get cooldown duration
  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 3) * 1000;

  //Check whether the author message is in timeout list
  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;
    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;
      return message.reply(
        `please wait ${timeLeft.toFixed(
          1
        )} more second(s) before reusing the \`${command.name}\` command.`
      );
    }
  }
  //Remove from timeout list
  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  //Execute command
  try {
    command.execute(message, args);
  } catch (error) {
    Log.addError(`${error.message} in ${TAG}`);
    message.reply("There was an error trying to execute that command!");
  }
});

client.login(token);
