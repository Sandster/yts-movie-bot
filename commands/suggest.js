const Log = require(`../utils/log`);
const Request = require(`../utils/request`);

module.exports = {
  name: `suggest`,
  description: `Used to get 4 suggestions to a selected movie`,
  cooldown: 10,
  argsMandatory: false,
  execute(message, args) {
    const TAG = `suggest.js`;
    let url = `https://yts.mx/api/v2/movie_suggestions.json?`;

    Log.addInfo(`Executing command: ${this.name} in ${TAG}`);
    args.forEach((movieId) => {
      if (args.length > 1) {
        url += `&movie_id=${movieId}`;
      } else {
        url += `movie_id=${movieId}`;
      }
    });

    Request.getData(url, message, TAG);
  },
};
