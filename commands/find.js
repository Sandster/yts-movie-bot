const axios = require("axios").default;
const Log = require(`../utils/log`);
const Request = require(`../utils/request`);

module.exports = {
  name: "find",
  description: "Find movie by name",
  guildOnly: true,
  cooldown: 10,
  argsMandatory: true,
  execute(message, args) {
    const TAG = `find.js`;
    Log.addInfo(`Executing command: ${this.name} in ${TAG}`);

    let movieName = ``;
    args.forEach((parts) => (movieName = movieName + ` ` + parts));

    movieName = movieName.trim();

    Request.getData(
      `https://yts.mx/api/v2/list_movies.json?query_term="${movieName}"&limit=6`,
      message,
      TAG
    );
  },
};
