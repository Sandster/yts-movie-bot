const { MessageEmbed } = require("discord.js");

module.exports = {
  displayEmbeddedMovie(movieData, message) {
    const genres = getGenres(movieData.genres);
    const quality = getAvailableQuality(movieData.quality);

    const newMovieEmbed = new MessageEmbed();
    newMovieEmbed.setTitle(`${movieData.title} (${movieData.year})`);
    newMovieEmbed.setURL(`${movieData.link}`);
    newMovieEmbed.addField(`Movie ID: `, `${movieData.id}`, true);
    newMovieEmbed.addField(`Rating: `, `${movieData.rating}`, true);
    newMovieEmbed.addField(`Genre(s): `, `${genres}`, true);
    newMovieEmbed.addFields({
      name: `Available in: `,
      value: `${quality}`,
    });
    newMovieEmbed.setColor(0x27ae60);
    newMovieEmbed.setThumbnail(
      `https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Logo-YTS.svg/1200px-Logo-YTS.svg.png`
    );
    newMovieEmbed.setImage(movieData.image);
    newMovieEmbed.setDescription(movieData.description);
    newMovieEmbed.setFooter(
      `Requested by ${message.author.username}`,
      `${message.author.displayAvatarURL()}`
    );
    newMovieEmbed.setTimestamp();
    message.channel.send(newMovieEmbed);
  },
  displayError(message, errorTitle, errorDescription) {
    const errorEmbed = new MessageEmbed();
    errorEmbed.setTitle(errorTitle);
    errorEmbed.setThumbnail(
      `https://tricks4english.files.wordpress.com/2014/04/no_sign.png`
    );
    errorEmbed.setColor(0xff0000);
    errorEmbed.setDescription(errorDescription);
    message.channel.send(errorEmbed);
  },
  displayWarning(message, warningTitle, warningDescription) {
    const warningEmbed = new MessageEmbed();
    warningEmbed.setTitle(warningTitle);
    warningEmbed.setThumbnail(
      `https://static.ariste.info/wp-content/uploads/2020/04/1200px-Antu_dialog-warning.svg_-1.png`
    );
    warningEmbed.setColor(0xf39c12);
    warningEmbed.setDescription(warningDescription);
    message.channel.send(warningEmbed);
  },
};

const getGenres = (movieGenres) => {
  let genres = ``;
  movieGenres.forEach((genre) => {
    genres === `` ? (genres = genre) : (genres = genres + `, ${genre}`);
  });

  return genres;
};

const getAvailableQuality = (availableVersions) => {
  let availableQuality = ``;
  availableVersions.forEach((version) => {
    let formattedVersion = ``;

    formattedVersion = version.type;

    switch (version.type) {
      case `bluray`:
        formattedVersion = `Blu-Ray`;
        break;
      case `web`:
        formattedVersion = `Web`;
        break;
      case undefined:
        formattedVersion = ``;
        break;
    }
    const trimmedVersion = formattedVersion.trim();
    availableQuality === ``
      ? (availableQuality = `${trimmedVersion} ${version.quality}`)
      : (availableQuality =
          availableQuality + `, ${trimmedVersion} ${version.quality}`);
  });

  return availableQuality;
};
