module.exports = class MovieDetails {
  constructor(
    title,
    id,
    year,
    description,
    link,
    rating,
    quality,
    genres,
    image
  ) {
    this.title = title;
    this.id = id;
    this.year = year;
    this.description = description;
    this.link = link;
    this.rating = rating;
    this.quality = quality;
    this.genres = genres;
    this.image = image;
  }
};
