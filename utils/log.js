const fs = require(`fs`);

module.exports = {
  addInfo(message) {
    const date = new Date();
    const log = `${date} INFO: ${message}`;
    console.log(log);

    const activityLog = fs.createWriteStream(`log.txt`, { flags: `a` });
    activityLog.write(log + `\n`);
  },
  addError(error) {
    const date = new Date();
    const log = `${date} ERROR: ${error}`;
    console.log(log);
    const activityLog = fs.createWriteStream(`log.txt`, { flags: `a` });
    activityLog.write(log + `\n`);
  },
  addWarn() {
    const date = new Date();
    const log = `${date} WARNING: ${message}`;
    console.log(log);
    const activityLog = fs.createWriteStream(`log.txt`, { flags: `a` });
    activityLog.write(log + `\n`);
  },
};
