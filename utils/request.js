const axios = require("axios").default;
const Log = require(`../utils/log`);
const MessagePlaceholder = require(`../components/SendMessage`);
const MovieDetails = require(`../modals/MovieDetails`);

module.exports = {
  getData(url, message, TAG) {
    axios
      .get(url)
      .then((response) => {
        if (response.data.data.movie_count === 0) {
          Log.addInfo(`No more movies found: ${TAG}`);
          MessagePlaceholder.displayError(
            message,
            `No results found`,
            `Please make sure the movie provided is valid`
          );
        } else {
          Log.addInfo(`Movies available in ${TAG}`);

          if (response.data.data.movie_count > 6) {
            MessagePlaceholder.displayWarning(
              message,
              `Too much results`,
              `It seems there's a lot of movies matching your criteria. We've filtered out the most relevant out for you. If you
              still can't find the movie you're looking for, try to be more specific with your search`
            );
          }
          response.data.data.movies.forEach((movieData) => {
            const movie = new MovieDetails(
              movieData.title,
              movieData.id,
              movieData.year,
              movieData.description_full,
              movieData.url,
              movieData.rating,
              movieData.torrents,
              movieData.genres,
              movieData.medium_cover_image
            );
            MessagePlaceholder.displayEmbeddedMovie(movie, message);
          });
        }
      })
      .catch((error) => {
        Log.addError(error);
        if (error.response !== null) {
          console.log(error.response.status);
          MessagePlaceholder.displayError(
            message,
            `Service unavailable`,
            `Please try again later`
          );
        }
      })
      .then(() => {
        Log.addInfo(`Request sent to server in ${TAG}`);
      });
  },
};
